#include "CutCallback.h"

#include <algorithm>

CutCallback::CutCallback( IloEnv& _env, string _cut_type, double _eps, Instance& _instance, IloBoolVarArray& _x, IloBoolVarArray& _z ) :
	LazyConsI( _env ), UserCutI( _env ), env( _env ), cut_type( _cut_type ), eps( _eps ), instance( _instance ), x( _x ), z( _z )
{
	arc_weights.resize( 2 * instance.n_edges );
}

CutCallback::~CutCallback()
{
}

void CutCallback::separate()
{
	if( cut_type == "dcc" ) connectionCuts();
	else if( cut_type == "cec" ) cycleEliminationCuts();
}

/*
 * separation of directed connection cut inequalities
 */
void CutCallback::connectionCuts()
{
	try {

		u_int n = instance.n_nodes;
        u_int m = instance.n_edges;

		IloNumArray xval( env, 2 * m );
		IloNumArray zval( env, n );

        if( lazy ) {
			LazyConsI::getValues( xval, x );
			LazyConsI::getValues( zval, z );

#if 0 //debug code
            std::cout << "Lazy" << std::endl;
            for (u_int i = 0; i < m; ++i) {
                if(1-eps <= xval[i] && xval[i] <= 1+eps) {
                    std::cout << i << "F(" << instance.edges[i].v1 << "," << instance.edges[i].v2 << ")" << std::endl;
                }
                if(1-eps <= xval[i+m] && xval[i+m] <= 1+eps) {
                    std::cout << i << "R(" << instance.edges[i].v2 << "," << instance.edges[i].v1 << ")" << std::endl;
                }
            }
            for (u_int j = 0; j < n; ++j) {
                if(1-eps <= zval[j] && zval[j] <= 1+eps) {
                    std::cout << j << ", ";
                }
            }
            std::cout << std::endl;
#endif

#ifndef MATHPROG_USELAZYFLOW //use Dijkstra to get cutset
            //build up weights according to current LP
            for (u_int i = 0; i < m; ++i) {
                if(1-eps <= xval[i] && xval[i] <= 1+eps) {
                    arc_weights[i] = 1; //active arc
                } else {
                    arc_weights[i] = std::numeric_limits<double>::max();
                }
                if(1-eps <= xval[i+m] && xval[i+m] <= 1+eps) {
                    arc_weights[i+m] = 1; //active arc
                } else {
                    arc_weights[i+m] = std::numeric_limits<double>::max();
                }
            }


            //build up set S and SS
            std::vector<u_int> setS(1);
            std::vector<u_int> setSS;
            std::vector<u_int> cutset;
            for (u_int i = 1; i < n; ++i) { //for each node
                if(1-eps <= zval[i] && zval[i] <= 1+eps) {
                    auto res = shortestPath(0, i);
                    if(res.weight == 0) {
                        setSS.push_back(i); //no path exists, store active node in SS
                    } else {
                        setS.push_back(i); //path exists, store active node in S
                    }
                } else {
                    setS.push_back(i); //store unused nodes in S
                }
            }

            //retrieve outgoing arcs for cutset
            for (u_int i : setS) {
                for (u_int j : instance.incidentEdges[i]) {
                    Instance::Edge& edge = instance.edges[j];

                    if(edge.v1 == i) {
                        //v2 is also in set S
                        if(std::find(setS.begin(), setS.end(), edge.v2) != setS.end())
                            continue;

                        cutset.push_back(j);
                    } else {
                        //v1 is also in set S
                        if(std::find(setS.begin(), setS.end(), edge.v1) != setS.end())
                            continue;

                        cutset.push_back(j+m);
                    }
                }
            }

            //add constraint
            for(u_int j : setSS) {
                IloExpr constraint(env);
                for (u_int i : cutset) {
                    constraint += x[i]; //note: i can be between [0, m[ or [m, 2m[
                }
                LazyConsI::add(z[j] <= constraint);
                constraint.end();
                break; //add only the first constraint
            }

#else //use MaxFlowMinCut to get cutset

            std::vector<double> capacities;
            std::list<std::pair<u_int, u_int> > arcs;
            //build up graph according to current LP
            for (u_int i = 0; i < m; ++i) {
                double c = std::max(0.0, xval[i]);
                capacities.push_back(c);
                arcs.push_back(std::make_pair(instance.edges[i].v1, instance.edges[i].v2));

                c = std::max(0.0, xval[i+m]);
                capacities.push_back(c);
                arcs.push_back(std::make_pair(instance.edges[i].v2, instance.edges[i].v1));
            }

            //compute maxflow
            std::vector<int> cut(instance.n_nodes);
            Maxflow mflow(instance.n_nodes, arcs.size(), arcs);
            mflow.update(0, 1, capacities.data());

            //create random order
            std::vector<u_int> order(n-1);
            std::iota(order.begin(), order.end(), 1);
            std::random_shuffle(order.begin(), order.end());

            for (u_int i : order) { //for each node in random order
                if(zval[i] <= eps)
                    continue; //dont compute min cut, since flow < zval[i]-eps == false

                mflow.update(0, i);
                double flow = mflow.min_cut(100, cut.data());

                if(flow < zval[i]-eps) { //flow violated
                    std::vector<u_int> setS;
                    std::vector<u_int> setSS;
                    std::vector<u_int> cutset;

                    //group cut
                    for(int i = 0; i < n; ++i) {
                        if(cut[i] == 1) {
                            setS.push_back(i);
                        } else {
                            setSS.push_back(i);
                        }
                    }

                    //retrieve outgoing arcs for cutset
                    for (u_int i : setS) {
                        for (u_int j : instance.incidentEdges[i]) {
                            Instance::Edge& edge = instance.edges[j];

                            if(edge.v1 == i) {
                                //v2 is also in set S
                                if(std::find(setS.begin(), setS.end(), edge.v2) != setS.end())
                                    continue;

                                cutset.push_back(j);
                            } else {
                                //v1 is also in set S
                                if(std::find(setS.begin(), setS.end(), edge.v1) != setS.end())
                                    continue;

                                cutset.push_back(j+m);
                            }
                        }
                    }

                    //add constraint
                    IloExpr constraint(env);
                    for (u_int i : cutset) {
                        constraint += x[i]; //note: i can be between [0, m[ or [m, 2m[
                    }
                    LazyConsI::add(z[i] <= constraint);
                    constraint.end();
                    break; //add only the first constraint
                }
            }
#endif
		}
		else {
            UserCutI::getValues( xval, x );
            UserCutI::getValues( zval, z );

#ifndef MATHPROG_NOUSERCUT
            std::vector<double> capacities;
            std::list<std::pair<u_int, u_int> > arcs;
            //build up graph according to current LP
            for (u_int i = 0; i < m; ++i) {
                double c = std::max(0.0, xval[i]);
                capacities.push_back(c);
                arcs.push_back(std::make_pair(instance.edges[i].v1, instance.edges[i].v2));

                c = std::max(0.0, xval[i+m]);
                capacities.push_back(c);
                arcs.push_back(std::make_pair(instance.edges[i].v2, instance.edges[i].v1));
            }

            //compute maxflow
            std::vector<int> cut(instance.n_nodes);
            Maxflow mflow(instance.n_nodes, arcs.size(), arcs);
            mflow.update(0, 1, capacities.data());

            //create random order
            std::vector<u_int> order(n-1);
            std::iota(order.begin(), order.end(), 1);
            std::random_shuffle(order.begin(), order.end());

            for (u_int i : order) { //for each node in random order
                if(zval[i] <= eps)
                    continue; //dont compute min cut, since flow < zval[i]-eps == false

                mflow.update(0, i);
                double flow = mflow.min_cut(100, cut.data());

                if(flow < zval[i]-eps) { //flow violated
                    std::vector<u_int> setS;
                    std::vector<u_int> setSS;
                    std::vector<u_int> cutset;

                    //group cut
                    for(int i = 0; i < n; ++i) {
                        if(cut[i] == 1) {
                            setS.push_back(i);
                        } else {
                            setSS.push_back(i);
                        }
                    }

                    //retrieve outgoing arcs for cutset
                    for (u_int i : setS) {
                        for (u_int j : instance.incidentEdges[i]) {
                            Instance::Edge& edge = instance.edges[j];

                            if(edge.v1 == i) {
                                //v2 is also in set S
                                if(std::find(setS.begin(), setS.end(), edge.v2) != setS.end())
                                    continue;

                                cutset.push_back(j);
                            } else {
                                //v1 is also in set S
                                if(std::find(setS.begin(), setS.end(), edge.v1) != setS.end())
                                    continue;

                                cutset.push_back(j+m);
                            }
                        }
                    }

                    //add constraint
                    IloExpr constraint(env);
                    for (u_int i : cutset) {
                        constraint += x[i]; //note: i can be between [0, m[ or [m, 2m[
                    }
                    if(0.99 < zval[i]-flow) //only add "most violated", else branch-bound
                        UserCutI::add(z[i] <= constraint);
                    constraint.end();
                    break; //add only the first constraint
                }
            }
#endif
        }

		xval.end();
		zval.end();

	}
	catch( IloException& e ) {
		cerr << "CutCallback: exception " << e.getMessage();
		exit( -1 );
	}
	catch( ... ) {
		cerr << "CutCallback: unknown exception.\n";
		exit( -1 );
	}
}

/*
 * separation of cycle elimination cut inequalities
 */
void CutCallback::cycleEliminationCuts()
{
	try {

		u_int n = instance.n_nodes;
		u_int m = instance.n_edges;

		IloNumArray xval( env, 2 * m );
		IloNumArray zval( env, n );

		if( lazy ) {
			LazyConsI::getValues( xval, x );
            LazyConsI::getValues( zval, z );

            //create random order
            std::vector<u_int> order(2*m);
            std::iota(order.begin(), order.end(), 0);
            std::random_shuffle(order.begin(), order.end());

            //build up weights according to current LP
            for (u_int i = 0; i < m; ++i) { //for each edge
                Instance::Edge& edge = instance.edges[i%m];

                if(edge.v1 == 0 || edge.v2 == 0) {
                    arc_weights[i] = 1; //exclude root arcs
                    arc_weights[i+m] = 1; //exclude root arcs
                    continue;
                }

                if(1-eps <= xval[i] && xval[i] <= 1+eps) {
                    arc_weights[i] = 0; //active arc
                } else {
                    arc_weights[i] = 1;
                }
                if(1-eps <= xval[i+m] && xval[i+m] <= 1+eps) {
                    arc_weights[i+m] = 0; //active arc
                } else {
                    arc_weights[i+m] = 1;
                }
            }

            for (u_int i : order) { //for each arc in random order
                if(1-eps <= xval[i] && xval[i] <= 1+eps)
                {//active arc
                    double backup_weight = arc_weights[i];
                    Instance::Edge& edge = instance.edges[i%m];

                    //skip root arcs
                    if(edge.v1 == 0 || edge.v2 == 0)
                        continue;

                    //delete arc
                    arc_weights[i] = std::numeric_limits<double>::max();

                    auto res = shortestPath(edge.v1, edge.v2);
                    if(res.path.size() >= 2 && res.weight == 0) {
                        //cycle found
                        IloExpr constraint(env);
                        constraint += x[i]; //add this arc
                        for(u_int i : res.path) { //add arcs in path
                            constraint += x[i]; //note: i can be between [0, m[ or [m, 2m[
                        }
                        IloNum Cmin1 = res.path.size();
                        LazyConsI::add(constraint <= Cmin1);
                        constraint.end();
                        break; //if there is no break, random order does not change result
                    }
                    arc_weights[i] = backup_weight;
                }
            }
		}
		else {
			UserCutI::getValues( xval, x );
			UserCutI::getValues( zval, z );

#ifndef MATHPROG_NOUSERCUT
            //create random order
            std::vector<u_int> order(2*m);
            std::iota(order.begin(), order.end(), 0);
            std::random_shuffle(order.begin(), order.end());

            //build up weights according to current LP
            for (u_int i = 0; i < m; ++i) { //for each edge
                Instance::Edge& edge = instance.edges[i%m];

                if(edge.v1 == 0 || edge.v2 == 0) {
                    arc_weights[i] = 1; //exclude root arcs
                    arc_weights[i+m] = 1; //exclude root arcs
                    continue;
                }

                arc_weights[i  ] = 1.0-(xval[i  ]-eps);
                arc_weights[i+m] = 1.0-(xval[i+m]-eps);
            }

            for (u_int i : order) { //for each arc in random order
                if(xval[i] <= eps)
                    continue; //inactive arc

                double backup_weight = arc_weights[i];
                Instance::Edge& edge = instance.edges[i%m];

                //skip root arcs
                if(edge.v1 == 0 || edge.v2 == 0)
                    continue;

                //delete arc
                arc_weights[i] = std::numeric_limits<double>::max();

                //arc (i, j) removed, check path (j, i)
                u_int a = edge.v2;
                u_int b = edge.v1;
                if(m<=i) {
                    a = edge.v1;
                    b = edge.v2;
                }

                auto res = shortestPath(a, b);
                if(res.path.size() >= 2 && res.weight + 1-(xval[i]-eps) < 1) {
                    //cycle found
                    IloExpr constraint(env);
                    constraint += x[i]; //add this arc
                    for(u_int i : res.path) { //add arcs in path
                        constraint += x[i]; //note: i can be between [0, m[ or [m, 2m[
                    }
                    IloNum Cmin1 = res.path.size();
                    UserCutI::add(constraint <= Cmin1);
                    constraint.end();
                    break; //if there is no break, random order does not change result
                }
                arc_weights[i] = backup_weight;
            }
#endif
        }

		xval.end();
		zval.end();

	}
	catch( IloException& e ) {
		cerr << "CutCallback: exception " << e.getMessage();
		exit( -1 );
	}
	catch( ... ) {
		cerr << "CutCallback: unknown exception.\n";
		exit( -1 );
	}
}

/*
 * Dijkstra's algorithm to find a shortest path
 * (slow implementation in time O(n^2))
 */
CutCallback::SPResultT CutCallback::shortestPath( u_int source, u_int target )
{
	u_int n = instance.n_nodes;
	u_int m = instance.n_edges;
	vector<SPNodeT> nodes( n );
	vector<bool> finished( n, false ); // indicates finished nodes

	// initialization
	for( u_int v = 0; v < n; v++ ) {
		nodes[v].pred = -1;
		nodes[v].pred_arc_id = -1;
		if( v == source ) nodes[v].weight = 0;
		else nodes[v].weight = numeric_limits<double>::max();
	}

	while( true ) {

		// find unfinished node with minimum weight to examine next
		// (should usually be done with heap or similar data structures)
		double wmin = numeric_limits<double>::max();
		u_int v;
		for( u_int u = 0; u < n; u++ ) {
			if( !finished[u] && nodes[u].weight < wmin ) {
				wmin = nodes[u].weight;
				v = u;
			}
		}

		// if all reachable nodes are finished
		// or target node is reached -> stop
		if( wmin == numeric_limits<double>::max() || v == target ) break;

		// this node is finished now
		finished[v] = true;

		// update all adjacent nodes on outgoing arcs
		list<u_int>::iterator it;
		for( it = instance.incidentEdges[v].begin(); it != instance.incidentEdges[v].end(); it++ ) {
			u_int e = *it; // edge id
			u_int a; // according arc id
			u_int u; // adjacent node
			if( instance.edges[e].v1 == v ) {
				a = e;
				u = instance.edges[e].v2;
			}
			else {
				a = e + m;
				u = instance.edges[e].v1;
			}
			// only examine adjacent node if unfinished
			if( !finished[u] ) {
				// check if weight at node u can be decreased
				if( nodes[u].weight > nodes[v].weight + arc_weights[a] ) {
					nodes[u].weight = nodes[v].weight + arc_weights[a];
					nodes[u].pred = v;
					nodes[u].pred_arc_id = a;
				}
			}
		}
	}

	SPResultT sp;
	sp.weight = 0;
	int v = target;
	while( v != (int) source && v != -1 ) {
		int a = nodes[v].pred_arc_id;
		if( a < 0 ) break;
		sp.weight += arc_weights[a];
		sp.path.push_back( a );
		v = nodes[v].pred;
	}
	return sp;
}

