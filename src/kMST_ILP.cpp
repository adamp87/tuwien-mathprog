#include "kMST_ILP.h"

kMST_ILP::kMST_ILP( Instance& _instance, string _model_type, int _k ) :
	instance( _instance ), model_type( _model_type ), k( _k )
{
	n = instance.n_nodes;
	m = instance.n_edges;
	if( k == 0 ) k = n;
}

void kMST_ILP::solve()
{
#ifdef MATHPROG_RANDOM
    // random seed from current time
    std::srand(std::time(0));
#endif

	// initialize CPLEX solver
	initCPLEX();

	// add common constraints
	modelCommon();

	// ++++++++++++++++++++++++++++++++++++++++++
	// TODO solve model
	// ++++++++++++++++++++++++++++++++++++++++++

	try {
		// build model
		cplex = IloCplex( model );
		//cplex.exportModel( "model.lp" );

		// set parameters
		epInt = cplex.getParam( IloCplex::EpInt );
		epOpt = cplex.getParam( IloCplex::EpOpt );
		setCPLEXParameters();

		// set cut- and lazy-constraint-callback for
		// cycle-elimination cuts ("cec") or directed connection cuts ("dcc")
		CutCallback* usercb = new CutCallback( env, model_type, epOpt, instance, x, z );
		CutCallback* lazycb = new CutCallback( env, model_type, epOpt, instance, x, z );
		cplex.use( (UserCutI*) usercb );
		cplex.use( (LazyConsI*) lazycb );

		// solve model
		cout << "Calling CPLEX solve ...\n";
		cplex.solve();
		cout << "CPLEX finished.\n\n";
		cout << "CPLEX status: " << cplex.getStatus() << "\n";
		cout << "Branch-and-Bound nodes: " << cplex.getNnodes() << "\n";
		cout << "Objective value: " << cplex.getObjValue() << "\n";
		cout << "CPU time: " << Tools::CPUtime() << "\n\n";

        std::fstream tex("results.tex", std::fstream::out | std::fstream::app);
        tex << " & " << cplex.getObjValue()
            << " & " << cplex.getNnodes()
            << " & " << cplex.getNcuts(IloCplex::CutUser)
            << " & " << Tools::CPUtime();

#if 0 //debug code
        for (u_int i = 0; i < m; ++i) {
            IloNum val = 0;
            try { val = cplex.getValue(x[i]); } catch(...) {}
            if(1-epInt <= val && val <= 1+epInt) {
                std::cout << i << "F(" << instance.edges[i].v1 << "," << instance.edges[i].v2 << ")" << std::endl;
            }
            val = 0;
            try { val = cplex.getValue(x[i+m]); } catch(...) {}
            if(1-epInt <= val && val <= 1+epInt) {
                std::cout << i << "R(" << instance.edges[i].v2 << "," << instance.edges[i].v1 << ")" << std::endl;
            }
        }
        for (u_int j = 0; j < n; ++j) {
            IloNum val = 0;
            try { val = cplex.getValue(z[j]); } catch(...) {}
            if(1-epInt <= val && val <= 1+epInt) {
                std::cout << j << ", ";
            }
        }
        std::cout << std::endl;
#endif
	}
	catch( IloException& e ) {
		cerr << "kMST_ILP: exception " << e.getMessage();
		exit( -1 );
	}
	catch( ... ) {
		cerr << "kMST_ILP: unknown exception.\n";
		exit( -1 );
	}
}

// ----- private methods -----------------------------------------------

void kMST_ILP::initCPLEX()
{
	cout << "initialize CPLEX ... ";
	try {
		env = IloEnv();
		model = IloModel( env );
		values = IloNumArray( env );
	}
	catch( IloException& e ) {
		cerr << "kMST_ILP: exception " << e.getMessage();
	}
	catch( ... ) {
		cerr << "kMST_ILP: unknown exception.\n";
	}
	cout << "done.\n";
}

void kMST_ILP::setCPLEXParameters()
{
	// print every line of node-log and give more details
	cplex.setParam( IloCplex::MIPInterval, 100 );
	cplex.setParam( IloCplex::MIPDisplay, 2 );
	// deactivate CPLEX general-purpose cuts
	//cplex.setParam( IloCplex::EachCutLim, 0 );
	//cplex.setParam( IloCplex::FracCuts, -1 );
	// only use a single thread
    cplex.setParam( IloCplex::Threads, 1 );
}

void kMST_ILP::modelCommon()
{
    x = IloBoolVarArray(env, 2*m); //binary array for arc selection
    z = IloBoolVarArray(env, n); //binary array for node selection

    for (u_int i = 0; i < m; ++i) {
        x[i]   = IloBoolVar(env, Tools::indicesToString("x", instance.edges[i].v1, instance.edges[i].v2).c_str());
        x[i+m] = IloBoolVar(env, Tools::indicesToString("x", instance.edges[i].v2, instance.edges[i].v1).c_str());
    }
    for (u_int j = 0; j < n; ++j) {
        z[j] = IloBoolVar(env, Tools::indicesToString("z", j).c_str());
    }

    //objective function
    IloExpr objective(env);
    for (u_int i = 0; i < m; ++i) {
        //exclude root arcs
        if(instance.edges[i].v1 == 0 || instance.edges[i].v2 == 0)
            continue;
        objective += x[i]   * instance.edges[i].weight;
        objective += x[i+m] * instance.edges[i].weight;
    }
    model.add(IloMinimize(env, objective));
    objective.end();

#if 0
    {//exactly k nodes, no root, implied by one outgoing arc
        IloExpr constraint(env);
        for( u_int j = 1; j < n; ++j) {
            constraint += z[j];
        }
        model.add(constraint == k);
        constraint.end();
    }
#endif

    {//exactly k-1 arcs, no root
        IloExpr constraint(env);
        for( u_int i = 0; i < m; ++i) {
            if(instance.edges[i].v1 == 0 || instance.edges[i].v2 == 0)
                continue;

            constraint += x[i];
            constraint += x[i+m];
        }
        model.add(constraint == k-1);
        constraint.end();
    }

    {//root node constraints
        IloExpr constraintij(env);
        IloExpr constraintji(env);
        for (u_int i : instance.incidentEdges[0]) {
            if(instance.edges[i].v1 == 0) {
                constraintij += x[i];
                constraintji += x[i+m];
            } else {
                constraintji += x[i];
                constraintij += x[i+m];
            }
        }
        model.add(z[0] == 0); //dummy, make z[0] extractable
        model.add(constraintij == 1); //exactly one outgoing arc for root
        model.add(constraintji == 0); //no incoming arcs, "exclude from model", dummy for extraction
        constraintij.end();
        constraintji.end();
    }

    for (u_int j = 1; j < n; ++j)
    {//node constraints
        IloExpr constraintij(env);
        IloExpr constraintji(env);
        for (u_int i : instance.incidentEdges[j]) {
            if(instance.edges[i].v1 == j) {
                constraintij += x[i];
                constraintji += x[i+m];
            } else {
                constraintji += x[i];
                constraintij += x[i+m];
            }
#ifndef MATHPROG_NOIMPROVE
            //ref: kct.pdf
            //no root arcs
            //implies one arc can be selected
            //implies inactive node cant have incident arcs
            //if arc is selected both nodes must be active
            if(instance.edges[i].v1 != 0 && instance.edges[i].v2 != 0)
                model.add(x[i]+x[i+m] <= z[j]);
#endif
        }

        model.add(constraintji == z[j]); //one incoming arc if selected, else zero
#ifdef MATHPROG_NOIMPROVE
        model.add(constraintij <= z[j]*(k-1)); //deselected node cant have out-arcs, else max
#endif
        constraintij.end();
        constraintji.end();
    }

#ifdef MATHPROG_NOIMPROVE
    for (u_int i = 0; i < m; ++i) {//only one arc can be selected
        IloExpr constraint(env);
        constraint += x[i];
        constraint += x[i+m];
        model.add(constraint <= 1);
        constraint.end();
    }
#endif
}

kMST_ILP::~kMST_ILP()
{
	// free CPLEX resources
	cplex.end();
	model.end();
	env.end();
}
