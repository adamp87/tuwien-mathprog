\documentclass[10pt]{article}
\usepackage{float}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage[colorlinks=true, urlcolor=blue]{hyperref}

\newcommand{\N}{\mathbb{N}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\Q}{\mathbb{Q}}
\newcommand{\vc}[1]{\boldsymbol{#1}}

\title{Mathematical Programming SS 2015\\
TU Wien\\
Programming Exercise}
\date{30.06.2015}
\author{Adam Papp 1327381 066 932 e1327381@student.tuwien.ac.at}

\begin{document}

\maketitle
\newpage

\section{Problem Description}

k-Node Minimum Spanning Tree (k-MST) Problem

\begin{itemize}
\item Given:
 \begin{itemize}
 \item (undirected) graph $G=(V, E, w)$
 \item nonnegative weighting function $w(e) \in \R^{+}_{0}, \;\; \forall e \in E$
 \item integer $k \leq |V|$
 \end{itemize}
\item Goal: Find a minimum weight tree, spanning exactly $k$ nodes.
\end{itemize}

\section{Formulation}

\begin{itemize}
\item $r$, artificial root node, $r \notin V$
\item $A = \{(i, j), (j, i) \; | \; \{i, j\} \in E\}$
\item $A_r = \{(r, v) \; | \; v \in V, w((r, v))=0\}$
\item $w((i, j)) := w((j, i)) := w(\{i, j\}) \;\; \forall \{i, j\} \in E$
\end{itemize}

\begin{figure}[H]
\begin{equation}
\min \sum_{a \in A} x_{a} w(a)
\end{equation}
\caption{Objective function, minimize the cost of arcs $a$ spanning the graph.}
\end{figure}

\begin{figure}[H]
\begin{equation}
\sum_{a \in A} x_{a} = k-1
\end{equation}
\caption{The tree must span $k$ nodes using $k-1$ arcs.}
\end{figure}

\begin{figure}[H]
\begin{equation}
\sum_{a \in A_r} x_{a} = 1
\end{equation}
\caption{The root node $r$ has exactly one outgoing arc.}
\end{figure}

\begin{figure}[H]
\begin{equation}
\sum_{a \in \sigma^{-}(v)} x_{a} = z_{v} \;\;\; \forall v \in V
\end{equation}
\caption{Active nodes must have one incoming arc, inactive nodes have no incoming arc. This constraint implies, that $k$ nodes must be selected. \cite{ktree}}
\end{figure}

\begin{figure}[H]
\begin{equation}
x_{(v, u)} + x_{(u, v)} \leq z_{v} \;\;\; \forall v \in V, \forall u \in \sigma^{-}(v) \cup \sigma^{+}(v), u \neq r
\label{eq:common}
\end{equation}
\caption{An arc can be selected only in one direction. Inactive nodes cannot have active incident arcs. If an arc is active, the node must be active also. \cite{ktree}}
\end{figure}

\begin{figure}[H]
\begin{equation}
x_{a} , z_{v} \in \{0, 1\} \;\;\; \forall a \in A \cup A_r, \forall v \in V
\end{equation}
\caption{Variable $x$: 1 if arc is included in spanning tree. Variable $z$: 1 if node is included in spanning tree.}
\end{figure}

\section{Directed cut constraint (DCC)}

\subsection{Formulation}

\begin{figure}[H]
\begin{equation}
\sum_{a \in \sigma^{+}(S)} x_{a} \geq z_{v} \;\;\; \forall S \subset V \cup \{r\}, r \in S, \forall v \in S^\complement
\label{eq:dcc}
\end{equation}
\caption{The directed cut formulation for the k spanning tree. It ensures at least one active arc between a subset $S, r \in S$ and the complement subset $S^\complement$, which has at least one active node.}
\end{figure}

\subsection{Lazy constraint separation}

Each time a lazy constraint callback happens, the separation is performed on an integer solution. These callbacks happen less frequent then user cut callbacks. The following algorithm was used for the integer separation:

\begin{itemize}
\item Observe each node $v$ with $z_v^* = 1$ else $S:= S \cup \{v\}$
\item Find path from $r$ to $v$ w.r.t. arcs $a \in A \;|\; x_a = 1$
\item If there is a path, then $S:= S \cup \{v\}$ else $S^\complement:= S^\complement \cup \{v\}$
\item After all $v$ has been observed, if $S^\complement \neq \emptyset$, inequality \eqref{eq:dcc} is added enforcing a path between $r$ and one of $v \in S^\complement$ and the separation stops
\end{itemize}

The path computation can be done by a depth-first search, but for simplicity the $\textit{shortestPath()}$ algorithm is used from the framework.

\subsection{User cut constraint separation}

User cut constraints cut only fractional feasible regions and no integer solutions. According to CPLEX documentation\footnote{\url{http://www-01.ibm.com/support/knowledgecenter/SS9UKU_12.6.1/com.ibm.cplex.zos.help/CPLEX/UsrMan/topics/progr_adv/usr_cut_lazy_constr/04_diffs.html}}, it is still possible to cut integer solutions if they are also cut by lazy constraints. Such cuts are called optimality-based cuts. When a cut callback happens, the separation is performed on a fractional solution by the following algorithm:

\begin{itemize}
\item Consider each node $v$ with $z_v^* > 0$
\item Compute $\{S, S^\complement\} := \textit{min-cut max-flow}$ from $r$ to $v$ w.r.t. arc capacities $x_a$
\item If $\sum_{a \in \sigma^{+}(S)} x_{a} < z_v^*$, inequality \eqref{eq:dcc} is added enforcing a path between $r$ and $v$ and the separation stops, note: equation $z_v^* - \sum_{a \in \sigma^{+}(S)} x_{a} > 0.99$ is used in implementation to add only "most violated" cut
\end{itemize}

\section{Cycle elimination constraint (CEC)}

\subsection{Formulation}

\begin{figure}[H]
\begin{equation}
\sum_{a \in C} x_{a} \leq |C|-1 \;\;\; \forall C \subseteq A, |C| \geq 2, C \text{ forms a cycle}
\label{eq:cec}
\end{equation}
\caption{Cycle elimination constraint.}
\end{figure}

\subsection{Separation}

For the separation of the CEC, the following algorithm was used:

\begin{itemize}
\item Consider each arc $a = (i, j)$ with $x_a^* > 0$
\item Set the weight of $x_a^*$ to some sufficiently large constant (deletion)
\item Compute shortest path from $j$ to $i$ w.r.t. arc weights $1-x_a$
\item If a path is found, and its cost plus $1-x_a^*$ is less than 1, inequality \eqref{eq:cec} is added enforcing these arcs "not to form a cycle" and the separation stops
\end{itemize}

When the separation is performed on an integer solution (lazy constraint), it would be possible to use a depth-first search to find a path between node $i$ and $j$, but for simplicity, the same algorithm was used with path from $i$ to $j$. In a fractional solution(user cut constraint), when using a single node to single node Dijkstra algorithm, computing a path from $j$ to $i$ is faster, since the algorithm stops as soon as $i$ is relaxed.

\newpage
\section{Results \& Conclusion}

The strength of the common formulation is important. Using constraint \eqref{eq:common}, the solutions were found significantly faster. The CEC formulation has a larger number of B\&B nodes, due to the fact, that it is a weaker formulation \cite{task4}. It was interesting to see, that when adding user cut constraint for the CEC formulation, the number of B\&B nodes have decreased, but due to the separation, the processing time has increased, table \ref{table:results} and \ref{table:resultsNoCut}. The order of adding violated inequalities shows different results in processing time, table \ref{table:results} and \ref{table:resultsRandom}. Note: both tables contain solutions in random order, but the random generator is initialized with time as seed in table \ref{table:resultsRandom}. More sophisticated selection of "most violated" constraints could improve the processing time. The solutions were found the fastest when using the DCC formulation.

\begin{figure}[h]
\centering
\begin{tabular}{|ll||llll||llll|}
\toprule
Name & $k$ & \multicolumn{4}{c}{DCC} & \multicolumn{4}{c}{CEC} \\
& & Result & B\&B & B\&C & Time(s) &
    Result & B\&B & B\&C & Time(s) \\
\midrule
g01 & 2 & 46 & 0 & 0 & 0.01 & 46 & 0 & 0 & 0 \\
g01 & 5 & 477 & 0 & 0 & 0.01 & 477 & 0 & 0 & 0.01 \\
g02 & 4 & 373 & 0 & 0 & 0.02 & 373 & 0 & 1 & 0.02 \\
g02 & 10 & 1390 & 0 & 3 & 0.01 & 1390 & 0 & 1 & 0.02 \\
g03 & 10 & 725 & 0 & 1 & 0.01 & 725 & 0 & 2 & 0.02 \\
g03 & 25 & 3074 & 0 & 3 & 0.08 & 3074 & 146 & 22 & 0.13 \\
g04 & 14 & 909 & 5 & 0 & 0.09 & 909 & 7 & 2 & 0.09 \\
g04 & 35 & 3292 & 0 & 7 & 0.14 & 3292 & 57 & 10 & 0.24 \\
g05 & 20 & 1235 & 0 & 7 & 0.12 & 1235 & 18 & 4 & 0.38 \\
g05 & 50 & 4898 & 0 & 16 & 0.06 & 4898 & 480 & 74 & 0.72 \\
g06 & 40 & 2068 & 14 & 29 & 0.81 & 2068 & 5031 & 91 & 9.99 \\
g06 & 100 & 6705 & 21 & 57 & 1.01 & 6705 & 2222 & 240 & 8.88 \\
g07 & 60 & 1335 & 0 & 5 & 0.38 & 1335 & 73 & 10 & 5.26 \\
g07 & 150 & 4534 & 7 & 26 & 0.93 & 4534 & 248 & 51 & 3.46 \\
g08 & 80 & 1620 & 10 & 72 & 6.48 & 1620 & 45 & 12 & 6.21 \\
g08 & 200 & 5787 & 37 & 74 & 5.54 & 5787 & 5121 & 521 & 90.36 \\
\bottomrule
\end{tabular}
\caption{Results of the described formulation and separation on \textit{behemoth.ads.tuwien.ac.at}.}
\label{table:results}
\end{figure}

\begin{figure}[h]
\centering
\begin{tabular}{|ll||llll||llll|}
\toprule
Name & $k$ & \multicolumn{4}{c}{DCC} & \multicolumn{4}{c}{CEC} \\
& & Result & B\&B & B\&C & Time(s) &
    Result & B\&B & B\&C & Time(s) \\
\midrule
g01 & 2 & 46 & 0 & 0 & 0.01 & 46 & 0 & 0 & 0.01 \\
g01 & 5 & 477 & 0 & 0 & 0 & 477 & 0 & 0 & 0 \\
g02 & 4 & 373 & 0 & 0 & 0.02 & 373 & 0 & 0 & 0.02 \\
g02 & 10 & 1390 & 0 & 3 & 0 & 1390 & 0 & 1 & 0.01 \\
g03 & 10 & 725 & 0 & 1 & 0.01 & 725 & 0 & 2 & 0.02 \\
g03 & 25 & 3074 & 0 & 3 & 0.09 & 3074 & 143 & 21 & 0.1 \\
g04 & 14 & 909 & 5 & 0 & 0.1 & 909 & 5 & 0 & 0.09 \\
g04 & 35 & 3292 & 6 & 10 & 0.17 & 3292 & 128 & 15 & 0.25 \\
g05 & 20 & 1235 & 0 & 10 & 0.15 & 1235 & 18 & 4 & 0.36 \\
g05 & 50 & 4898 & 32 & 13 & 0.21 & 4898 & 469 & 68 & 0.5 \\
g06 & 40 & 2068 & 316 & 38 & 1.48 & 2068 & 7044 & 92 & 9.57 \\
g06 & 100 & 6705 & 551 & 71 & 1.68 & 6705 & 3873 & 227 & 5.88 \\
g07 & 60 & 1335 & 1 & 10 & 3.71 & 1335 & 165 & 18 & 5.64 \\
g07 & 150 & 4534 & 38 & 36 & 0.84 & 4534 & 304 & 48 & 4.17 \\
g08 & 80 & 1620 & 40 & 8 & 0.9 & 1620 & 86 & 7 & 6.19 \\
g08 & 200 & 5787 & 3514 & 221 & 29.77 & 5787 & 8613 & 457 & 45.01 \\
\bottomrule
\end{tabular}
\caption{Results of the described formulation and separation without user cut constraints on \textit{behemoth.ads.tuwien.ac.at}.}
\label{table:resultsNoCut}
\end{figure}

\begin{figure}[h]
\centering
\begin{tabular}{|ll||llll||llll|}
\toprule
Name & $k$ & \multicolumn{4}{c}{DCC} & \multicolumn{4}{c}{CEC} \\
& & Result & B\&B & B\&C & Time(s) &
    Result & B\&B & B\&C & Time(s) \\
\midrule
g01 & 2 & 46 & 0 & 0 & 0 & 46 & 0 & 0 & 0.01 \\
g01 & 5 & 477 & 0 & 0 & 0.01 & 477 & 0 & 0 & 0.01 \\
g02 & 4 & 373 & 0 & 0 & 0.01 & 373 & 0 & 1 & 0.01 \\
g02 & 10 & 1390 & 0 & 3 & 0 & 1390 & 0 & 1 & 0.02 \\
g03 & 10 & 725 & 0 & 1 & 0.02 & 725 & 0 & 2 & 0.02 \\
g03 & 25 & 3074 & 0 & 3 & 0.02 & 3074 & 146 & 22 & 0.14 \\
g04 & 14 & 909 & 5 & 0 & 0.09 & 909 & 7 & 2 & 0.09 \\
g04 & 35 & 3292 & 0 & 6 & 0.07 & 3292 & 57 & 10 & 0.23 \\
g05 & 20 & 1235 & 0 & 7 & 0.13 & 1235 & 18 & 4 & 0.37 \\
g05 & 50 & 4898 & 19 & 21 & 0.25 & 4898 & 480 & 74 & 0.74 \\
g06 & 40 & 2068 & 30 & 29 & 0.59 & 2068 & 5031 & 91 & 10.08 \\
g06 & 100 & 6705 & 0 & 49 & 0.59 & 6705 & 2222 & 240 & 9.1 \\
g07 & 60 & 1335 & 0 & 5 & 0.4 & 1335 & 73 & 10 & 5.34 \\
g07 & 150 & 4534 & 7 & 30 & 1.01 & 4534 & 265 & 51 & 3.79 \\
g08 & 80 & 1620 & 4 & 17 & 1.13 & 1620 & 45 & 12 & 6.29 \\
g08 & 200 & 5787 & 50 & 91 & 7.93 & 5787 & 4359 & 499 & 78.43 \\
\bottomrule
\end{tabular}
\caption{Results of the described formulation and separation in random node/arc order on \textit{behemoth.ads.tuwien.ac.at}.}
\label{table:resultsRandom}
\end{figure}

\clearpage
\bibliographystyle{plain}
\bibliography{ref}
\end{document}

